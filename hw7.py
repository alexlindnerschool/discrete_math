Dom = range(5)

Rel = [ ]
for i in Dom:
  for j in Dom:
    if i == j:
      Rel.append((i,j))
    elif i == 4-j:
      Rel.append((i,j))

def Reflexive(A,R):
  for x in A:
    if (x,x) not in R:
      return False
  return True

def Symmetric(R):
  for (a,b) in R:
    if (b,a) not in R:
      return False
  return True

def Transitive(R):
  for (a,b) in R:
    for (c,d) in R:
      if (b == c) and ((a,d) not in R):
        return False
  return True

print("R =",Rel)
print(60*"*")
print("Reflexive(A,R) =",Reflexive(Dom,Rel))
print(60*"*")
print("Symmetric(R) =",Symmetric(Rel))
print(60*"*")
print("Transitive(R) =",Transitive(Rel))
