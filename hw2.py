#A
hyps= [ 'p <= (not q)','(not r) or q','r']

concl= 'not p'

fst= "{0:^3s}|{1:^3s}|{2:^3s}|{3:^7s}|{4:^6s}|{5:^3s}|{6:^4s}"
fst2 = "{0:^3d}|{1:^3d}|{2:^3d}|{3:^7d}|{4:^6d}|{5:^3d}|{6:^4d}"
div = "---+---+---+-------+------+---+----"

print(fst.format('p','q','r','p->~q','~r|q','r','~p'))

for p in range(2):
    for q in range(2):
        for r in range(2):
            print(div)
            print (fst2.format(p,q,r, eval(hyps[0]),
                  eval(hyps[1]), eval(hyps[2]),eval(concl)))

print()

#B

hyps= ['p <= r','q <= r']

concl= '(p and q) <= r'

fst= "{0:^3s}|{1:^3s}|{2:^3s}|{3:^6s}|{4:^6s}|{5:^8s}"
fst2 = "{0:^3d}|{1:^3d}|{2:^3d}|{3:^6d}|{4:^6d}|{5:^8d}"
div = "---+---+---+------+------+--------"

print(fst.format('p','q','r','p->r','q->r','p&q->r'))

for p in range(2):
    for q in range(2):
        for r in range(2):
            print(div)
            print (fst2.format(p,q,r, eval(hyps[0]),
                  eval(hyps[1]), eval(concl)))

