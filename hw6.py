A = ['a','b','c']
B = ['b','c','d']
C = ['b','c','e']

def union(x,y):
  Z = []
  add = True
  for n in x:
    for i in Z:
      if n==i:
        add = False
    if add==True:
      Z+=n
    else:
      add = True
  for n in y:
    for i in Z:
      if n==i:
        add = False
    if add==True:
      Z+=n
    else:
      add = True
  return Z

def intersection(x,y):
  Z = []
  add = True
  for n in x:
    for i in y:
      if n==i:
        for j in Z:
          if n==j:
            add = False
        if add==True:
          Z+=n
        else:
          add = True
  return Z

num1 = intersection(A,union(B,C))
num2 = union(intersection(A,B),C)
num3 = union(intersection(A,B),intersection(A,C))
ans1 = ""
ans2 = ""
ans3 = ""
for n in num1:
  ans1+=n+","
for n in num2:
  ans2+=n+","
for n in num3:
  ans3+=n+","
print("#1:{"+ans1+"\b}")
print("#2:{"+ans2+"\b}")
print("#3:{"+ans3+"\b}")
