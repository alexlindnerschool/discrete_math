def trial_division(n):
    a = []
    while n%2 == 0:
        a.append(2)
        n /= 2
    f = 3
    while f * f <= n:
        if n % f == 0:
            a.append(f)
            n /= f
        else:
            f += 2
    if n != 1: a.append(n)
    return a
num = input("Enter a positive integer: ")    
a = trial_division(637425403705125)
i = 0
while i < len(a):
    count = 1;
    j = 0
    while j < len(a):
        if i != j:
            if  a[i] == a[j]:
                count+=1
        j+=1
    if count != 0:
        print(a[i],"^",count, sep='',end=" ")
    if i + count >= len(a):
        print()
        break
    else:
        print("+",end=" ")
    i += count
