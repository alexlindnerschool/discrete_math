Dom = ['a','b','c','d']

Rel = [('c','b'), ('b','a'), ('c','a')]
for i in Dom:
  for j in Dom:
    if i == j or j == 'd':
      Rel.append((i,j))

def Reflexive(D,R):
  for x in D:
    if (x,x) not in R:
      return False
  return True

def Transitive(R):
  for (a,b) in R:
    for (c,d) in R:
      if (b == c) and ((a,d) not in R):
        return False
  return True

def AntiSymmetric(R):
  for (a,b) in R:
    if (a !=b) and ((b,a) in R):
      return False
  return True

def PartialOrder(D,R):
  return Reflexive(D,R) and \
    AntiSymmetric(R) and \
    Transitive(R)

def TotalOrder(D,R):
  if PartialOrder(D,R):
    for i in D:
      for j in D:
        if (i,j) not in R and (j,i) not in R:
          return False
    return True
  return False

print("Rel =",Rel)
print(60*"*")
print("AntiSymmetric(Rel) =",AntiSymmetric(Rel))
print(20*"*")
print("PartialOrder(D,Rel) =",PartialOrder(Dom,Rel))
print(20*"*")
print("TotalOrder(D,Rel) =",TotalOrder(Dom,Rel))
