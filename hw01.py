#A
hyps= [ 'p <= (not q)','(not r) or q','r']

concl= 'not p'

fst= "{0:^3s}|{1:^3s}|{2:^3s}|{3:^7s}|{4:^6s}|{5:^3s}|{6:^4s}"
fst2 = "{0:^3d}|{1:^3d}|{2:^3d}|{3:^7d}|{4:^6d}|{5:^3d}|{6:^4d}"
div = "---+---+---+-------+------+---+----"

print(fst.format('p','q','r','p->~q','~r|q','r','~p'))

for p in range(2):
    for q in range(2):
        for r in range(2):
            print(div)
            print (fst2.format(p,q,r, eval(hyps[0]),
                  eval(hyps[1]), eval(hyps[2]),eval(concl)))

print()

#B

hyps= ['p <= r','q <= r']

concl= '(p and q) <= r'

fst= "{0:^3s}|{1:^3s}|{2:^3s}|{3:^6s}|{4:^6s}|{5:^8s}"
fst2 = "{0:^3d}|{1:^3d}|{2:^3d}|{3:^6d}|{4:^6d}|{5:^8d}"
div = "---+---+---+------+------+--------"

print(fst.format('p','q','r','p->r','q->r','p&q->r'))

for p in range(2):
    for q in range(2):
        for r in range(2):
            print(div)
            print (fst2.format(p,q,r, eval(hyps[0]),
                  eval(hyps[1]), eval(concl)))

#2
sum=0;
for i in range(2,16):
    sum+=1/i
    print(1/i)
print(sum)

#3
def pickWord(word):
    newString = word[1:] + word[0] + "ay"
    return newString
x = pickWord("Run")
y = pickWord("a")
print(x)
print(y)

#4
def isTriangle(a, b, c):
    if((a+b)<=c):
        makesTriangle = False
    elif((a+c)<=b):
        makesTriangle = False
    elif((b+c)<=a):
        makesTriangle = False
    else:
        makesTriangle = True
    print(makesTriangle)
def getInput():
    num1 = int(input("Enter the first stick length: "))
    num2 = int(input("Enter the second stick length: "))
    num3 = int(input("Enter the third stick length: "))
    isTriangle(num1, num2, num3)
getInput()

#5
def altDiff(a):
    difference = a[-1]
    for i in a[-2::-1]:
        difference = i-difference
    print(difference)
altDiff([17,5,8,10])
altDiff([6,8,7])
altDiff([4,5])



