people = ['Clark','Daw','Fuller']
global painter, carpenter, plumber

def notKnow(p1, p2): #p1 does not know p2
    global painter, carpenter, plumber
    if p1 == p2:
        return False
    if p1 == carpenter or p2 == carpenter:
        return False
    if not(p1 == 'Fuller' and p2 == 'Daw'):
        return False
    if (p1 == painter and p2 == plumber):
        return False
    return True
    
    
for painter in people:
    for carpenter in people:
        for plumber in people:
            sol = notKnow(plumber,painter) and notKnow('Fuller','Daw')
            if sol:
                print("Painter =",painter," Carpenter =",\
                    carpenter," Plumber =",plumber)
