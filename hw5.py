def f(n):
    if n==1:
        return 1
    else:
        return (n*(5*n-3))/2
def g(n):
    if n==1:
        return 1
    else:
        return (5*n-4)+g(n-1)

for n in range(1,11):
    print("n=%2d f(n)=%7d  g(n)=%7d" % (n,f(n),g(n)))
